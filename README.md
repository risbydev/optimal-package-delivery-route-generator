# Optimal Delivery Route Generator

## About
- C950 - DSA2 project
- This application calculates an optimal delivery route for the packages using _Nearest Neighbor_ algorithm

<details>
  <summary>Scenario</summary>
  
  ![image](https://user-images.githubusercontent.com/14286113/190919299-53454eac-0195-43fb-9526-be493361968f.png)
  
</details>

## Built with
- Python

## Demo

<details>
  <summary>Generate delivery route, deliver packages, and display delivery statistics</summary>
 
  ![image](https://user-images.githubusercontent.com/14286113/187777703-5ffb408c-ff21-4aa4-a993-60718749440f.png)
</details>

<details>
  <summary>View individual package delivery information</summary>
  
  ![image](https://user-images.githubusercontent.com/14286113/187777810-b57748e8-cab7-4efe-abc4-10960fefd6d1.png)
</details>

<details>
  <summary>View delivery status of all packages and trucks at specific points in time</summary>
  
  ![image](https://user-images.githubusercontent.com/14286113/187777900-76809c89-9463-43b9-80c4-b5e472a2b354.png)
</details>



