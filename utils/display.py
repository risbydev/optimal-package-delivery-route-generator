class Color:
    '''Values for formatting console output'''
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[31m'
    ENDC = '\033[0m' # end format
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Text:
    '''Convenience functions for formatting text'''
    
    # O(1)
    @staticmethod
    def add_space_when_single_digit(input: int):
        '''Add extra space in front of single digit number to make it aligned with 2 digit numbers'''
        input_str = str(input)
        input_str = (" " + input_str) if len(input_str)==1 else input_str
        
        return input_str

    # O(1)
    @staticmethod
    def create_blank_spaces(num: int):
        return " " * num