import csv

from model.package import Package
from model.hashmap import HashMap
from model.address import Address, address_lookup

# O(N)
def load_csv(file:str):
    '''Takes in file path and outputs file contents as list of strings'''
    res = []
    with open(file, 'r') as data:
        reader = csv.reader(data)
        next(reader) # skip first row

        for row in reader:
            res.append(row)

    return res

# O(N)
def load_address_csv(file:str):
    '''Takes in file path to addresses csv and outputs file contents as an address list'''
    raw = load_csv(file)
    addresses = []

    # raw = 0:id,1:name,2:street,3:city,4:state,5:zip
    for address in raw:
        new_address = Address(int(address[0]),address[1],address[2],address[3],address[4],address[5])
        addresses.append(new_address)

    return addresses

# O(N*M)
def load_package_csv(file: str, addresses: list[Address]) -> HashMap:
    '''Takes in file path to packages csv and address list, and outputs packages hashmap'''
    raw = load_csv(file)
    packages = HashMap(len(raw))

    # raw = 0:Package ID,1:Address,2:City,3:State,4:Zip,5:Delivery Deadline,6:Mass KILO,7:Special Notes
    for package in raw:
        new_address = address_lookup(package[1], addresses) # O(M)
        new_package = Package(int(package[0]),new_address,package[5],package[6],package[7])
        packages.put(new_package.id, new_package)
        
    return packages