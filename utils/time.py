from datetime import datetime, timedelta

class Time:
    '''Utility methods to handle time'''

    # O(1)
    @staticmethod
    def to_datetime(time:str) -> datetime:
        '''Takes in time in format of "00:00 AM" and outputs the datetime version of that'''
        return datetime.strptime(time, "%I:%M %p")

    # O(1)
    @staticmethod
    def to_str(time:datetime) -> str:
        '''Takes in datetime and outputs formatted string version "00:00 AM"'''
        return time.strftime("%I:%M %p")
    
    # O(1)
    @staticmethod
    def increment_hours(current: datetime, hours: float):
        '''Takes in datetime and hours and outputs the datetime incremented by hours'''
        return current + timedelta(hours=hours)

    # O(1)
    @staticmethod
    def is_greater(time: datetime|str, time2: datetime|str) -> bool:
        '''Takes in time1 and time2 and outputs whether time1 > time2'''
        if(isinstance(time, str)):
            time = Time.to_datetime(time)

        if(isinstance(time2, str)):
            time2 = Time.to_datetime(time2)

        return time.time() > time2.time()
    
    # O(1)
    @staticmethod
    def is_equal(time: datetime| str, time2: datetime|str) -> bool:
        '''Takes in in time1 and time2 and outputs whether time1 == time2'''
        if(isinstance(time, str)):
            time = Time.to_datetime(time)

        if(isinstance(time2, str)):
            time2 = Time.to_datetime(time2)

        return time.time() == time2.time()

    # O(1)
    @staticmethod 
    def is_greater_or_equal(time: datetime| str, time2: datetime|str) -> bool:
        return Time.is_greater(time,time2) or Time.is_equal(time,time2)