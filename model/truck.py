import sys
from model.address import HUB, distance_between

from utils.time import Time
from utils.display import Color, Text
from model.package import Package
from model.stats import TruckStats

class Truck:
    '''A class that represents the delivery truck'''
    PACKAGE_LIMIT = 16

    def __init__(self, id: int, distances):
        self.id = id
        self.distances = distances
        self.packages: list[Package] = []
        self.route = []
        self.stats = TruckStats()

    # O(1)
    def load(self, package: Package):
        '''Takes in package and adds it to internal packages list; Throws exception if package is added beyond capacity'''
        if package.loaded:
            raise Exception("Package has already been loaded!")
        
        if len(self.packages) < self.PACKAGE_LIMIT:
            package.loaded = True        
            package.truck_id = self.id 
            self.packages.append(package)
        else:
            raise Exception("Package limit reached!")

    # O(N)
    def _verify_packages(self):
        '''Checks for violations in packages as described in package notes and updates package departure time and status'''
        for p in self.packages:
            message_template = f'\nPackage notes: {p.notes}\nPackage #{p.id}'

            if p.truck_id == -1:
                raise Exception(f'Package id not set!')

            if p.notes.lower() == "can only be on truck 2":
                if self.id != 2:
                    raise Exception(f'{message_template} must be on Truck #2')
            
            if p.notes.lower() == "delayed on flight---will not arrive to depot until 9:05 am":
                if Time.is_greater("09:05 AM", self.stats.travel_time):
                    raise Exception(f'{message_template} must be leave the hub after 9:05 AM\nWas loaded at {self.stats.get_travel_time_str()}')

            if p.notes.lower() == "must be delivered with 13, 15":
                if 13 not in self.packages or 15 not in self.packages:
                    raise Exception(f'{message_template} must be on the same truck as packages #13 and #15')

            if p.notes.lower() == "must be delivered with 13, 19":
                if 13 not in self.packages or 19 not in self.packages:
                    raise Exception(f'{message_template} must be on the same truck as packages #13 and #19')

            if p.notes.lower() == "must be delivered with 15, 19":
                if 15 not in self.packages or 19 not in self.packages:
                    print(f'truck #{self.id}')
                    raise Exception(f'{message_template} must be on the same truck as packages #15 and #19')

            p.departure_time = self.stats.departure_time
            
    # O(N^2)
    def deliver(self):
        '''Delivers packages while updating delivery time on packages and truck stats (mileage and time)'''

        self.stats.set_departure_time(self.stats.get_travel_time_str())              # Set truck departure time
        self._verify_packages()                                                      # Validate packages 
        
        # O(N^2)
        self.packages = Truck.get_nearest_packages(self.distances, self.packages)  # Get distance-optimized route with nearest_neighbor algo
        self.route.append(HUB)                                                     # Mainly used to retrieve last destination for calculating distance

        print(f'\n{Color.YELLOW}{self.stats.get_travel_time_str()}: Truck #{self.id} leaving delivery hub{Color.ENDC}\n')

        for current_package in self.packages:
            current_address = self.route[-1]                                                                 # Get current location 
            
            distance = distance_between(current_address, current_package.address, self.distances)            # Get distance between current location and next package location
            
            self.stats.update(distance)                                                                      # Increment truck mileage and travel time

            current_package.status = Package.STATUS_DELIVERED
            current_package.set_delivered_at(self.stats.travel_time)

            self.route.append(current_package.address)

            print(f'{self.stats.get_travel_time_str()} - Truck #{self.id} - Package #{Text.add_space_when_single_digit(current_package.id)} -> {current_package.address.name}')

        # return to hub
        last_package = self.packages[-1]

        distance = distance_between(last_package.address, HUB, self.distances)
        self.stats.update(distance)

        self.route.append(HUB)

        self.stats.set_return_time(self.stats.get_travel_time_str())

        print(f'\n{Color.BLUE}{self.stats.get_return_time_str()}: Truck #{self.id} returning to delivery hub{Color.ENDC}')
        print("---------------------------------------------------------------------------------------------------")


    # Self-Adjusting Heuristic Algorithm: "nearest neighbor" greedy algorithm
    # Scalable element: Packages
    # O(N^2)
    @staticmethod
    def get_nearest_packages(distances: list[list[int]], packages: list[Package]) -> list[Package]:
        '''Takes in distance matrix and list of packages and outputs partially optimal package delivery order by distance using nearest neighbor algorithm'''
        nearest_packages = []   # visited packages
        nearest_package = None

        current_address = HUB

        while(len(nearest_packages) != len(packages)):       # stop loop when all packages have been visited
            min_distance = sys.maxsize

            for package in packages:
                if package.status == Package.STATUS_HUB:     # Package.STATUS_HUB = unvisited
                    distance = distance_between(current_address, package.address, distances)
                    if distance < min_distance:
                        min_distance = distance              # Current smallest distance to current package
                        nearest_package = package            # Current closest package to current package

            nearest_package.status = Package.STATUS_EN_ROUTE # Package.STATUS_EN_ROUTE = visited
            nearest_packages.append(nearest_package)         # Update visited packages list
            current_address = nearest_package.address        # Change current package to be the next closest package


        return nearest_packages