from datetime import datetime

from model.package import Package
from utils.time import Time

class TruckStats:
    '''A class that represents a trucks delivery statistics'''

    MILEAGE_LIMIT = 140
    SPEED = 18 # mph

    def __init__(self, time:str = "08:00 AM"):
        self.departure_time = None
        self.return_time = None
        self.travel_time = Time.to_datetime(time)
        self.eod = Time.to_datetime("05:00 pm")
        self.mileage = 0

    # O(1)
    def update(self, miles: float):
        '''Convenience function that increments trucks mileage and time after delivery'''
        self._increment_mileage(miles)
        self._increment_time(miles/ self.SPEED) # hours

    # O(1)
    def _increment_mileage(self, miles: float):
        '''Takes in miles and increments current truck mileage; Throws Exception if mileage exceeds limit'''
        mileage = self.mileage + miles

        if mileage > self.MILEAGE_LIMIT:
            raise Exception(f'Mileage can not exceed {self.MILEAGE_LIMIT} miles')

        self.mileage = mileage
    
    # O(1)
    def _increment_time(self, hours:float):
        '''Takes in hours and increments current truck delivery time'''
        self.set_travel_time(Time.increment_hours(self.travel_time, hours))

    # O(1)
    def get_mileage_str(self):
        '''Returns mileage rounded to 2 decimal places as a string'''
        return f'{round(self.mileage, 2)} miles'

    # O(1)
    def get_departure_time_str(self) -> str:
        '''Returns formatted Truck departure time or "At the Hub" if not departed yet'''
        return Time.to_str(self.departure_time) if self.departure_time else Package.STATUS_HUB

    # O(1)
    def set_departure_time(self, time:str|datetime):
        '''Takes in and sets departure time, as well as updates current time, and raises exception if it is not past end of day'''
        time_obj = time

        if isinstance(time, str):
            time_obj = Time.to_datetime(time)

        if Time.is_greater(time_obj, self.eod):
            raise Exception(f'Can not deliver after {Time.to_str(self.eod)}')

        self.departure_time = time_obj
        self.travel_time = time_obj

    # O(1)
    def get_return_time_str(self) -> str:
        '''Returns formatted Truck return time or "En Route" and "At the Hub" messages depending on departure time'''
        if(self.departure_time):
            return Time.to_str(self.return_time) if self.return_time else Package.STATUS_EN_ROUTE
        return Time.to_str(self.departure_time) if self.departure_time else Package.STATUS_HUB

    # O(1)
    def set_return_time(self, time:str|datetime):
        '''Takes in and sets return time, and raises exception if it is past end of day'''
        time_obj = time

        if isinstance(time, str):
            time_obj = Time.to_datetime(time)

        if Time.is_greater(time_obj, self.eod):
            raise Exception(f'Truck returned to hub late! Did not return by {Time.to_str(self.eod)}')

        self.return_time = time_obj
        # did not update travel_time since set_return_time is updated by current time in the first place

    # O(1)
    def get_travel_time_str(self) -> str:
        '''Returns current delivery time as a string'''
        return Time.to_str(self.travel_time)

    # O(1)
    def set_travel_time(self, time:str|datetime):
        '''Sets current delivery time and throws exception if time is past EOD'''
        time_obj = time

        if isinstance(time, str):
            time_obj = Time.to_datetime(time)

        if Time.is_greater(time_obj, self.eod):
            raise Exception(f'Can not deliver after {Time.to_str(self.eod)}')
        
        self.travel_time = time_obj

    # O(1)
    def __str__(self):
        return f'Departure Time: {self.get_departure_time_str()}\nReturn Time: {self.get_return_time_str()}\nCurrent Mileage: {self.get_mileage_str()}'