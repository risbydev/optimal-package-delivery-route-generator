from datetime import datetime
from model.address import Address
from model.hashmap import HashMap
from utils.time import Time
from utils.display import Color, Text


class Package:
    '''A class to represent packages'''
    # STATUS_NOT_READY = "DELAYED"
    STATUS_HUB = "AT THE HUB"
    STATUS_EN_ROUTE = "EN ROUTE"
    STATUS_DELIVERED = "DELIVERED"

    def __init__(self, id: int, address: Address, deadline:str, weight:str, notes:str):
        self.id = id
        self.address = address
        self._set_deadline(deadline)
        self.weight = weight
        self.notes = notes
        self.loaded = False
        self.status = self.STATUS_HUB
        self.departure_time:datetime = None
        self.delivered_at:datetime = None
        self.truck_id = -1

    def _set_deadline(self, deadline):
        '''Handles EOD deadlines by converting it to 05:00 PM'''
        if deadline == "EOD":
            self.deadline = "05:00 PM"
        else:
            self.deadline = deadline

    def set_delivered_at(self, time: str|datetime):
        '''Sets delivered_at time and raises error if past the deadline'''
        deadline = Time.to_datetime(self.deadline)
        
        if(isinstance(time, str)):
            time = Time.to_datetime(time)

        if Time.is_greater(time, deadline):
            raise Exception(f'Package was delivered late')

        self.delivered_at = time

    def get_status(self, lookup_time: str|datetime):
        '''Report package status for given time'''

        if(isinstance(lookup_time, str)):
            lookup_time = Time.to_datetime(lookup_time) # convert string to datetime

        if Time.is_greater_or_equal(lookup_time, self.delivered_at): 
            package_str = f'DELIVERED: ({Time.to_str(self.delivered_at)}) >>> '
        elif Time.is_greater_or_equal(lookup_time, self.departure_time):
            package_str = "EN ROUTE" + Text.create_blank_spaces(13) + " >>> "
        else:
            package_str = "AT THE HUB" + Text.create_blank_spaces(11) + " >>> "
    
        package_str += f'#{Text.add_space_when_single_digit(self.id)}, [{self.address.street} | {self.address.city}, {self.address.state} {self.address.zip}], {self.weight}kg, ({self.deadline})'
        
        return package_str

    def __str__(self):
        return f'id: {self.id}\ndeadline: {self.deadline}\nweight: {Text.add_space_when_single_digit(self.weight)}\nnotes: {self.notes}\nstatus: {self.status}\ndelivered at: {Time.to_str(self.delivered_at)}\naddress:\n{self.address}\n'

    def __repr__(self):
        return str(self)

    def __eq__(self, otherPackage):
        return self.id == otherPackage

    def __hash__(self):
        return self.id

 # O(N)
def update_packages(old_packages: HashMap, new_packages: list[Package]) -> HashMap:
    '''Replaces old_packages with updated, delivered packages'''
    for package in new_packages:
        old_packages.put(package.id, package)

    return old_packages

# O(N)
def status_lookup_by_time(lookup_time: str| datetime, packages: list[Package]):
    '''Takes in time and packages and outputs package statuses at that time'''
    
    print()
    for package in packages:
        status = package.get_status(lookup_time)

        if status[0].lower() == "d":    # DELIVERED
            print(f'{Color.CYAN}{status}{Color.ENDC}')
        elif status[0].lower() == "e":  # EN ROUTE
            print(f'{Color.BLUE}{status}{Color.ENDC}')
        elif status[0].lower() == "a":  # AT THE HUB
            print(f'{Color.YELLOW}{status}{Color.ENDC}')
        else:
            raise Exception('Unexpected package status!')
    print()