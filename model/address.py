class Address:
    '''A class to represent an address'''
    
    def __init__(self, id: int, name: str, street: str, city: str, state: str, zip: str):
        self.id = id
        self.name = name
        self.street = self.transform_street(street)
        self.city = city
        self.state = state
        self.zip = zip

    # O(n)
    @staticmethod
    def transform_street(street):
        '''Takes in and transforms street and outputs streets containing N,E,S,W to be North, East, South, West'''
        street_split = street.split()
        new_street = []

        for word in street_split:
            match word.upper():
                case 'N':
                    word = "North"
                case 'E':
                    word = "East"
                case 'S':
                    word = "South"
                case 'W':
                    word = "West"
                
            new_street.append(word)
        
        return ' '.join(new_street)

    def __str__(self):
        return f'street: {self.street}\ncity: {self.city}\nstate: {self.state}\nzip: {self.zip}'

    def __repr__(self):
        return str(self) + "\n"

# End Address class

# Begin Address Utils

HUB = Address(0, "Western Governors University", "4001 South 700 East", "Salt Lake City", "UT", "84107")

# O(1)
def distance_between(address1: Address, address2: Address, distances: list[list[int]]):
    '''Takes 2 addresses and address distance matrix and outputs the distance between the 2 addresses'''
    return float(distances[address1.id][address2.id]) if address1.id >= address2.id else float(distances[address2.id][address1.id]) # Ensure correct indices are used to prevent index out of bounds error

# O(N)
# Used to associate addresses from addressCSV to packages via address id
def address_lookup(street: str, addresses: list[Address]):
    '''Takes in street and addresses and outputs the address or raises an exception if no match is found'''
    standardized_street = Address.transform_street(street) # sanitize street

    for address in addresses:
        if address.street == standardized_street:
            return address

    raise Exception(f'Address lookup failed: {standardized_street} not found')