class HashMap:
    '''
    A class to represent HashMap based on List implementation
    '''
    def __init__(self, size):
        '''Size is the number of buckets'''
        self.size = size
        self.buckets = [[] for i in range(size)]

    # O(1)
    def _compute_index(self, key) -> int:
        '''Takes key and outputs the bucket index for that key in the mapping list'''
        return hash(key) % self.size-1

    # O(N)
    def put(self, key, value):
        '''Takes key value pair, storing it in buckets, and outputs the old value at the key if it was already present or None'''
        index = self._compute_index(key)

        if self.buckets[index]:                       # if bucket[index] is not empty
            for kv in self.buckets[index]:            # for every key-value list in bucket[index]
                if kv[0] == key:                      # if key already exists, then replace old value with new value
                    oldVal = kv[1]
                    kv[1] = value
                    return oldVal

        self.buckets[index].append([key, value])      # if bucket[index] is empty or key is not present, add key-value pair list to bucket
        return None

    # O(N)
    def get(self, key):
        '''Takes in key and outputs matching value if key is found or None'''
        index = self._compute_index(key)
        # source for list comprehension: https://stackoverflow.com/a/44807252/12369650
        results = [kv[1] for kv in (self.buckets[index] or []) if kv[0] == key] # list comprehension for finding key-value pair

        return None if not results else results[0]                              # return None if value does not exist for key or return value if it exists

    # O(N)
    def remove(self, key):
        '''Takes in key and removes the key value pair, outputting the removed value if key is found or None'''
        index = self._compute_index(key)

        if self.buckets[index]:
            for i, kv in enumerate(self.buckets[index]):
                if kv[0] == key:                 
                    oldValue = kv[1]             
                    self.buckets[index].pop(i) # delete key-value pair
                    return oldValue

        return None

    # O(N^2)
    def keys(self):
        '''Returns keys in hashmap as a list'''
        keys = []

        for bucket in self.buckets:
            if bucket:
                for kv in bucket:
                    keys.append(kv[0])

        return keys

    # O(N^2)
    def values(self):
        '''Returns values in hashmap as a list'''
        values = []

        for bucket in self.buckets:
            if bucket:
                for kv in bucket:
                    values.append(kv[1])

        return values

    # O(N^2)
    def __str__(self):
        contents = ""

        for arr in self.buckets:
            for kv in arr:
                contents += "{\nkey: "+str(kv[0])+"\n\nvalue: \n"+str(kv[1])+"\n}\n\n"

        return contents.rstrip(contents[-1]) # removes trailing newline