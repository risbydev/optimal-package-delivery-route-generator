from model.package import status_lookup_by_time, update_packages
from utils.display import Color
from utils.data import *
from model.truck import Truck
from utils.time import Time

# load data 
addresses = load_address_csv("./data/addressCSV.csv")
distances = load_csv("./data/distanceCSV.csv")

# Declare globals
packages: HashMap = None
truck1: Truck = None
truck2: Truck = None
truck3: Truck = None

# O(1)
def init_trucks():
    '''Initializes Truck objects with ID and distance values'''
    global truck1, truck2, truck3

    truck1 = Truck(1,distances)
    truck2 = Truck(2,distances)
    truck3 = Truck(3,distances)

# O(N*M)
def init_packages():
    '''Initializes package hashmap'''
    global packages

    packages = load_package_csv("./data/packageCSV.csv", addresses)

# O(1)
def load_trucks():
    '''Manual loading of packages to ensure their special requirements are fulfilled'''
    # ---- No deadlines / Deliver after 10:20 AM ----
    truck1.load(packages.get(2))
    truck1.load(packages.get(4))
    truck1.load(packages.get(5))
    truck1.load(packages.get(7))
    truck1.load(packages.get(8))
    truck1.load(packages.get(9)) # After 10:20 AM
    truck1.load(packages.get(10))
    truck1.load(packages.get(11))
    truck1.load(packages.get(12))
    truck1.load(packages.get(17))
    truck1.load(packages.get(21))
    truck1.load(packages.get(22))
    truck1.load(packages.get(23))
    truck1.load(packages.get(24))
    truck1.load(packages.get(26))
    truck1.load(packages.get(27))

    # ---- Must be on Truck 2 ----
    truck2.load(packages.get(3))
    truck2.load(packages.get(18))
    truck2.load(packages.get(36))
    truck2.load(packages.get(38))
    # ---- Must be on same truck ----
    truck2.load(packages.get(13)) # deadline 10:30am
    truck2.load(packages.get(14)) # deadline 10:30am
    truck2.load(packages.get(15)) # deadline 9:00am
    truck2.load(packages.get(16)) # deadline 10:30am
    truck2.load(packages.get(19))
    truck2.load(packages.get(20)) # deadline 10:30am
    # ---- Must be delivered by 10:30 AM ----
    truck2.load(packages.get(29))
    truck2.load(packages.get(30))
    truck2.load(packages.get(31))
    truck2.load(packages.get(34))
    truck2.load(packages.get(37))
    truck2.load(packages.get(40))

    # ---- Delayed packages (9:05 AM) ----
    truck3.load(packages.get(6))  # deadline 10:30am
    truck3.load(packages.get(25)) # deadline 10:30am
    truck3.load(packages.get(28))
    truck3.load(packages.get(32))
    # ---- Left over packages ----
    truck3.load(packages.get(1))  # deadline 10:30am
    truck3.load(packages.get(33))
    truck3.load(packages.get(35))
    truck3.load(packages.get(39))

# O(N)
def deliver_packages():
    '''Invoking deliver() on Trucks and manually setting departure time to ensure package special requirements are met'''
    delayed_packages_start_time = "09:06 AM" # Delayed packages; deliver after 9:05 AM 

    # First Driver
    truck2.deliver()

    # Second Driver
    truck3.stats.set_travel_time(delayed_packages_start_time)
    truck3.deliver()

    # Determine Truck 1 Delivery Start time
    t2_return_time = truck2.stats.return_time
    t3_return_time = truck3.stats.return_time
    t1_start_time = "10:21 AM" # Package 9 correction at 10:20AM

    both_drivers_return_after_t1_start_time = Time.is_greater(t2_return_time, t1_start_time) and Time.is_greater(t3_return_time, t1_start_time)
    driver_1_returned_earlier_than_driver_2 = Time.is_greater(t3_return_time, t2_return_time)
    
    # Pick driver who comes back first after 10:20AM to deliver truck 1 packages    
    if both_drivers_return_after_t1_start_time:
        if driver_1_returned_earlier_than_driver_2:
            truck1.stats.set_travel_time(t2_return_time) # first driver delivers truck1
        else:
            truck1.stats.set_travel_time(t3_return_time) # second driver delivers truck1
    else:                                                # one or both drivers return early
        truck1.stats.set_travel_time(t1_start_time)

    truck1.deliver()

# O(1)
def print_truck_stats():
    '''Outputs Truck statistics and total miles driven by all trucks'''
    print(f'\nDelivery Statistics:\n')

    print(f'{Color.BOLD}{truck1.stats}{Color.ENDC}\n')
    print(f'{Color.BOLD}{truck2.stats}{Color.ENDC}\n')
    print(f'{Color.BOLD}{truck3.stats}{Color.ENDC}\n')

    total = truck1.stats.mileage + truck2.stats.mileage + truck3.stats.mileage

    print(f'{Color.GREEN}Total miles driven: {total}{Color.ENDC}\n')

# O(N)
def update_package_delivery_info():
    '''Updates packages hashmap with their delivery information'''
    global packages

    packages = update_packages(packages, truck1.packages)
    packages = update_packages(packages, truck2.packages)
    packages = update_packages(packages, truck3.packages)

# O(N^2)
def start_delivery_simulation():
    '''Convenience method to invoke delivery simulation methods in order'''
    init_trucks()                  # O(1)
    init_packages()                # O(N*M)
    load_trucks()                  # O(N)
    deliver_packages()             # O(N^2)
    print_truck_stats()            # O(1)
    update_package_delivery_info() # O(N)

# O(N)
def package_menu():
    '''Console menu after delivery simulation begins'''
    while True:
        print()
        print("1. Look up package by ID")
        print("2. Look up packages by time")
        print("3. Go back to previous menu")
        choice = input("\n")

        if choice == "1":
            package_lookup_by_id()
            continue
        elif choice == "2":
            package_lookup_by_time()
            continue
        elif choice == "3":
            return 
        elif choice and choice[0].lower() == 'q':
            menu_exit_program()
        else:
            print("\nInvalid input! Please enter valid option or Q to quit")
            continue

# O(N)
def package_lookup_by_id():
    '''Console menu to look up package statuses by id'''
    while True:
        print("\nEnter package ID from 1-40 or '0' to go back to previous menu")
        id = input("\n")

        if id and id[0].lower() == "q":
            menu_exit_program()
        elif id and id[0].lower() == "b":
            break
        elif id == "0":
            break
        else:
            try:
                id_validated = int(id)
                if id_validated > 40 or id_validated < 1:
                    raise ValueError('Input of out range!')
                print(f'\nPackage status:\n')
                print(packages.get(id_validated))
                print("---------------------------------------------------------------------------------------------------")

            except ValueError:
                print(f'\n{Color.YELLOW}Invalid id! Please enter ID number from 1 to 40{Color.ENDC}')
                print("Enter 'back' to go back to the previous menu")
                print("Enter 'quit' to exit the program")
                continue


# O(N)
def package_lookup_by_time():
    '''Console menu to look up package statuses by time'''

    time_1 = "09:00 AM"
    time_2 = "10:00 AM"
    time_3 = "01:00 PM"

    while True:
        print("\nChoose the following preset times or enter your in own in the format of '00:00 AM'\n")
        print(f'1. {time_1}')
        print(f'2. {time_2}')
        print(f'3. {time_3}')
        print(f'4. Back to previous menu')
        time = input("\n")

        if time and time[0].lower() == "q":
            menu_exit_program()
        elif time and time[0].lower() == "b":
            break
        elif time == "1":
            print()
            print(f'Displaying package status at {time_1}')
            status_lookup_by_time("9:00 AM", packages.values())
            break
        elif time == "2":
            print()
            print(f'Displaying package status at {time_2}')
            status_lookup_by_time("10:00 AM", packages.values())
            break
        elif time == "3":
            print()
            print(f'Displaying package status at {time_3}')
            status_lookup_by_time("1:00 PM", packages.values())
            break
        elif time == "4":
            break
        else:
            try:
                time_validated = Time.to_datetime(time)
                print()
                print(f'Displaying package status at {time_validated}')
                status_lookup_by_time(time_validated, packages.values())
                return
            except ValueError:
                print(f'\n{Color.YELLOW}Invalid time! Please enter time in format of "00:00 AM" or choose from the preset options{Color.ENDC}')
                print("Enter 'back' to go back to the previous menu")
                print("Enter 'quit' to exit the program")
                continue

# O(1)
def menu_exit_program():
    '''Convenience method to exit the program'''
    print("Exiting program...")
    quit()    

# O(N^2)
def main():
    '''Main method to start program and display main menu'''
    while True:
        print(f'{Color.GREEN}---Welcome to Delivery Simulation---{Color.ENDC}\n')
        print("1. Start Program")
        print("2. Quit")
        choice = input("\n")

        if choice == "1":
            start_delivery_simulation() # O(N^2)
            print(f'{Color.UNDERLINE}Delivery simulation complete!{Color.ENDC}')
            package_menu()
        elif choice and choice[0].lower() == "q" or choice == "2":
            menu_exit_program()
        else:
            print(f'{Color.RED}Invalid input! Please enter valid option or Q to quit {Color.ENDC}')
            continue

main() # Start application